OpenEmbedded
============

![OpenEmbedded](/stands/openembedded/logo.png)

OpenEmbedded (OE) is a build system for Embedded Linux and more! Using
cross-compilation OE allows efficient development for target devices with
limited CPU, RAM and storage resources. We support building for everything from
IoT devices to desktop and server hardware. OE can produce ready-to-use images,
individual packages (RPM, DEB or IPK formats), package feeds and any other
artifacts you need.

Project Resources
-----------------

* [OpenEmbedded website](https://www.openembedded.org/wiki/Main_Page)

* [Source Code](https://git.openembedded.org/)

* [Mailing Lists](https://lists.openembedded.org/)

* IRC: #oe on FreeNode
